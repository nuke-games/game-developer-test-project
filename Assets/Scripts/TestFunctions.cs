using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using NukeGs;
using Utilities;

public static class TrigonometryTest
{
    public static void SetSpherePositionsOnCircle(List<GameObject> sphereObjects, float radian, float circleRadius, float rotationOffset)
    {
        
    }
}

public static class AlgorightmTest
{
    public static void OnGoldParticleCollected(int remainingParticleCount, int remainingGoldAmount, out int gainedGoldAmount)
    {
        gainedGoldAmount = 0; //Calculate the gained gold amount
    }
}

public static class UnityTest
{
    public static void HandleObjectsMovement(List<UnityTestSphereData> sphereDatas, Rect wallRect, UnityTestUpdateMode updateMode)
    {
        //Move objects here

        for (var i = 0; i < sphereDatas.Count - 1; i++)
        {
            CheckObjectCollisionWithOtherObjects(sphereDatas[i], sphereDatas.GetRange(i + 1, sphereDatas.Count - i - 1));
        }
        
        sphereDatas.ForEach(sphere =>
        {
            CheckObjectCollisionWithWalls(sphere, wallRect);
        });
    }

    private static void CheckObjectCollisionWithOtherObjects(UnityTestSphereData sphere, List<UnityTestSphereData> otherSpheres)
    {
        
    }

    private static void CheckObjectCollisionWithWalls(UnityTestSphereData sphere, Rect wallRect)
    {
        
    }
}


