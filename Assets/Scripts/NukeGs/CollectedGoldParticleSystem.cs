using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class CollectedGoldParticleSystem : MonoBehaviour
{
    private PoolManager _gainedGoldTextPool;

    [SerializeField] private GameObject gainedGoldText;

    private void Awake()
    {
        _gainedGoldTextPool = new PoolManager();
        _gainedGoldTextPool.SetUnits(new List<GameObject> {gainedGoldText});
    }

    public void GenerateParticle(int gainedGoldAmount)
    {
        if (gainedGoldAmount <= 0) return;
        
        var gainedGoldItem = _gainedGoldTextPool.GetObjectFromPool(0, transform);
        
        gainedGoldItem.GetComponent<GainedGoldTextEffect>().Setup(gainedGoldAmount, () =>
        {
            _gainedGoldTextPool.AddObjectToPool(gainedGoldItem);
        });
    }
}
