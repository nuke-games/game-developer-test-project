using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    [Header("Trigonometry Test Values")] 
    [SerializeField] private Slider radiusSlider;
    [SerializeField] private Slider radianBetweenItemsSlider;
    [SerializeField] private Slider rotationOffsetSlider;

    [Header("Algorithm Test Values")] 
    [SerializeField] private Button collectGoldButton;

    [SerializeField] private CollectedGoldParticleSystem collectedGoldParticleSystem;
    [SerializeField] private TextMeshProUGUI collectedGoldAmountText, totalParticleCountText, totalGoldAmountText;

    public event Action<float, float, float> OnTrigonometryTestValuesChanged;

    public event Action OnCollectGoldClicked;


    private void Awake()
    {
        radiusSlider.onValueChanged.AddListener(radius => TrigonometryTestValueChanged());
        radianBetweenItemsSlider.onValueChanged.AddListener(radian => TrigonometryTestValueChanged());
        rotationOffsetSlider.onValueChanged.AddListener(offset => TrigonometryTestValueChanged());
        
        collectGoldButton.onClick.AddListener(() => OnCollectGoldClicked?.Invoke());
    }

    private void Start()
    {
        TrigonometryTestValueChanged();
    }

    private void TrigonometryTestValueChanged()
    {
        OnTrigonometryTestValuesChanged?.Invoke(radiusSlider.value, radianBetweenItemsSlider.value,
            rotationOffsetSlider.value);
    }

    public void CollectGoldButtonSetActive(bool active)
    {
        collectGoldButton.interactable = active;
    }

    public void SetupGoldValues(int particleCount, int goldAmount)
    {
        totalParticleCountText.text = "Particles: " + particleCount;
        totalGoldAmountText.text = "Golds: " + goldAmount;
        
        UpdateCollectedGoldValue(0, 0);
    }

    public void UpdateCollectedGoldValue(int collectedCoin, int increaseAmount)
    {
        collectedGoldAmountText.text = collectedCoin.ToString();
        collectedGoldParticleSystem.GenerateParticle(increaseAmount);
    }
}
