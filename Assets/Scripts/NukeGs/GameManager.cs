using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace NukeGs
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private UiManager uiManager;

        [SerializeField] private List<GameObject> trigonometryTestObjects;

        [SerializeField] private GoldParticleSystem goldParticleSystem;

        [SerializeField] private Transform unityTestParent;
        [SerializeField] private Camera unityTestCamera;

        private List<UnityTestSphereData> _unityTestSphereDatas = new List<UnityTestSphereData>();

        private int _particleCount, _goldAmount, _collectedGoldAmount;

        private UnityTestUpdateMode _updateMode = UnityTestUpdateMode.Update;
        
        private void Awake()
        {
            uiManager.OnTrigonometryTestValuesChanged += UpdateTrigonometryTestObjects;
            uiManager.OnCollectGoldClicked += ActivateCollectGoldAnimation;

            SetupGoldValues();
            InitializeUnityTest();

            StartCoroutine(ChangeUpdateMode());
        }

        private IEnumerator ChangeUpdateMode()
        {
            yield return new WaitForSeconds(4f);
            _updateMode = _updateMode == UnityTestUpdateMode.Update
                ? UnityTestUpdateMode.FixedUpdate
                : UnityTestUpdateMode.Update;

            yield return ChangeUpdateMode();
        }

        public void ChangeTest()
        {
            unityTestCamera.gameObject.SetActive(!unityTestCamera.gameObject.activeInHierarchy);
        }

        private void UpdateTrigonometryTestObjects(float circleRadius, float radian, float rotationOffseet)
        {
            TrigonometryTest.SetSpherePositionsOnCircle(trigonometryTestObjects, radian * Mathf.PI, circleRadius, rotationOffseet * Mathf.PI);
        }

        private void ActivateCollectGoldAnimation()
        {
            uiManager.CollectGoldButtonSetActive(false);

            goldParticleSystem.StartGoldParticle(_particleCount, null, OnParticleCollected, SetupGoldValues);
        }

        private void OnParticleCollected(int remainingParticleCount)
        {
            AlgorightmTest.OnGoldParticleCollected(remainingParticleCount, _goldAmount - _collectedGoldAmount, out var gainedGoldFromParticle);

            _collectedGoldAmount += gainedGoldFromParticle;
        
            uiManager.UpdateCollectedGoldValue(_collectedGoldAmount, gainedGoldFromParticle);
        }

        private void SetupGoldValues()
        {
            _collectedGoldAmount = 0;
        
            _particleCount = Random.Range(20, 50);
            _goldAmount = Random.Range(100, 400);

            uiManager.SetupGoldValues(_particleCount, _goldAmount);
        
            uiManager.CollectGoldButtonSetActive(true);
        }

        private void InitializeUnityTest()
        {
            _unityTestSphereDatas.Clear();

            for (var i = 0; i < unityTestParent.childCount; i++)
            {
                var sphereTransform = unityTestParent.GetChild(i);
                var sphereData = new UnityTestSphereData(sphereTransform);
                sphereData.SetupObject(Random.Range(.5f, .8f), Random.insideUnitCircle * Random.Range(2f, 3f));
                
                _unityTestSphereDatas.Add(sphereData);
            }
        }

        private void Update()
        {
            if (_updateMode == UnityTestUpdateMode.Update)
            {
                UnityTest.HandleObjectsMovement(_unityTestSphereDatas, UnityTestWallRect(), UnityTestUpdateMode.Update);
            }
        }
        
        private void FixedUpdate()
        {
            if (_updateMode == UnityTestUpdateMode.FixedUpdate)
            {
                UnityTest.HandleObjectsMovement(_unityTestSphereDatas, UnityTestWallRect(), UnityTestUpdateMode.FixedUpdate);
            }
        }

        private Rect UnityTestWallRect()
        {
            var screenSize = new Vector2(unityTestCamera.orthographicSize * unityTestCamera.aspect * 2, unityTestCamera.orthographicSize * 2);
            
            return new Rect((Vector2)unityTestParent.position - screenSize * .5f, screenSize);
        }
    }
    
    [Serializable]
    public class UnityTestSphereData
    {
        public Transform Transform { get; }
        public float Radius { get; private set; }
        public float Mass { get; private set; }
        public Vector2 Velocity { get; private set; }

        public UnityTestSphereData(Transform transform)
        {
            Transform = transform;
        }

        public void SetupObject(float radius, Vector2 velocity)
        {
            Radius = radius;
            Velocity = velocity;
            Transform.localScale = radius * 2 * Vector3.one;
            Mass = Radius * Radius * Radius;
        }

        public void UpdateVelocity(Vector2 velocity)
        {
            Velocity = velocity;
        }
    }

    public enum UnityTestUpdateMode
    {
        Update, FixedUpdate
    }
}
