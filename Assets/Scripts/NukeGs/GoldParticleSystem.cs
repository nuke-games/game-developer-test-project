﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;

public class GoldParticleSystem : MonoBehaviour
{
    private PoolManager _goldParticlePool;

    public RectTransform destinationPosition;

    public GameObject goldParticle;

    private bool _isActive = false;
        
    private readonly Dictionary<RectTransform, Vector2> _activeGoldParticles = new Dictionary<RectTransform, Vector2>();

    private float _distanceToGo;

    private Action _onCompletedCallback;
    private Action<int> _onParticleCompletedCallback;

    private void Awake()
    {
        _goldParticlePool = new PoolManager();
        _goldParticlePool.SetUnits(new List<GameObject> {goldParticle});
    }

    public void StartGoldParticle(int particleCount, Action onAnimationBegin, Action<int/*remaining ParticleCount*/> onParticleCompleted, Action onFinishCallback)
    {
        var canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;

        this.UpdateForSeconds(.5f, (dt, ct) => { canvasGroup.alpha = ct * 2; })
            .OnComplete(
                () =>
                {
                    BeginAnimation(transform.position, particleCount, onAnimationBegin, onParticleCompleted,
                        onFinishCallback);
                });
    }

    private void Stop()
    {
        this.WaitForSeconds(2f).OnComplete(() =>
        {
            var canvasGroup = GetComponent<CanvasGroup>();
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
            canvasGroup.alpha = 0;

            _onCompletedCallback();
            _isActive = false;
        });
    }

    private void BeginAnimation(Vector2 startPosition, int particleCount, Action onAnimationBegin, Action<int/*remaining ParticleCount*/> onParticleCompleted,  Action onFinishedCallback)
    {
        _onCompletedCallback = onFinishedCallback;
        _onParticleCompletedCallback = onParticleCompleted;

        var destVector = ((Vector2)destinationPosition.transform.position - startPosition);
        _distanceToGo = destVector.magnitude;
            
        for (int i = 0; i < particleCount; i++)
        {
            var particle = _goldParticlePool.GetObjectFromPool(0, transform).GetComponent<RectTransform>();

            var goldParticleInitialVelocity = _distanceToGo * .4f * (new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) - destVector.normalized);

            particle.transform.position = startPosition;

            var localPosition = particle.transform.localPosition;
            localPosition.z = 0;
            particle.transform.localPosition = localPosition;
                
            _activeGoldParticles.Add(particle, goldParticleInitialVelocity);
        }

        _isActive = true;
            
        onAnimationBegin?.Invoke();
    } 

    private void Update()
    {
        if (!_isActive) return;
            
        var keys = _activeGoldParticles.Keys.ToArray();

        foreach (var particle in keys)
        {
            var previousPosition = (Vector2) particle.transform.position;

            var velocity = _activeGoldParticles[particle];
            particle.transform.position += (Vector3) velocity * Time.deltaTime;

            var distance = (Vector2)(destinationPosition.transform.position - particle.transform.position);
            var previousDistance = ((Vector2) destinationPosition.transform.position - previousPosition);
                
            var previousVelocity = velocity;
            velocity += _distanceToGo *
                        Time.deltaTime * 1.1f * distance.normalized;

            var projectedVector = velocity.ProjectedVector(distance);
            var vectorComponent = velocity - projectedVector;

            velocity -= 2 * Time.deltaTime * vectorComponent;
                
            particle.transform.localScale = Mathf.Pow(distance.magnitude / _distanceToGo, .2f)  * Vector3.one;

            _activeGoldParticles[particle] = velocity;
                
            if (Vector2.Dot(velocity, distance) <= 0 && Vector2.Dot(previousVelocity, previousDistance) >= 0)
            {
                _onParticleCompletedCallback?.Invoke(_activeGoldParticles.Count);
                    
                _activeGoldParticles.Remove(particle);
                _goldParticlePool.AddObjectToPool(particle.gameObject);

                if (_activeGoldParticles.Count == 0)
                {
                    Stop();
                }
            }
        }
    }
}