using System;
using System.Collections;
using TMPro;
using UnityEngine;
using Utilities;

public class GainedGoldTextEffect : MonoBehaviour
{
    private TextMeshProUGUI _gainedGoldText;

    private Action _onCompleted;

    private void Awake()
    {
        _gainedGoldText = GetComponent<TextMeshProUGUI>();
    }

    public void Setup(int gainedGoldAmount, Action onCompleted)
    {
        _gainedGoldText.text = "+" + gainedGoldAmount;

        _onCompleted = onCompleted;
        this.HandleCoroutine(MoveAndFade());
    }


    private IEnumerator MoveAndFade()
    {
        float animTime = 1f;
        float startTime = Time.time;

        float deltaTime = Time.time - startTime;
        
        while (deltaTime < animTime)
        {
            transform.position += Vector3.up * Time.deltaTime * .2f;
            
            var currentColor = _gainedGoldText.color;
            currentColor.a = Mathf.Lerp(1, 0, deltaTime / animTime);
            _gainedGoldText.color = currentColor;

            yield return null;
            
            deltaTime = Time.time - startTime;
        }
        
        _onCompleted?.Invoke();
    }
    
}
