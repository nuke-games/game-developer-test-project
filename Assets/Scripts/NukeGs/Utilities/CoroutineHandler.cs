﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public delegate bool UpdateWhileDelegate(float deltaTime, float elapsedTime);
    public delegate bool WaitUntilDelegate();

    public class CoroutineState
    {
        private readonly IEnumerator _coroutine;
        
        public bool Running { get; private set; }
        public bool Paused { get; private set; }
        public bool Stopped { get; private set; }
        
        private Action _onFinished;

        public CoroutineState(MonoBehaviour coroutineObject, IEnumerator coroutine)
        {
            _coroutine = coroutine;

            Start(coroutineObject);
        }
		
        public void Pause()
        {
            Paused = true;
        }
		
        public void Resume()
        {
            Paused = false;
        }
		
        private void Start(MonoBehaviour coroutineObject)
        {
            Running = true;
            coroutineObject.StartCoroutine(CallWrapper());
        }
		
        public void Stop()
        {
            Stopped = true;
            Running = false;
        }
		
        private IEnumerator CallWrapper()
        {
            int startedFrame = Time.frameCount;
            
            while(Running) {
                if(Paused)
                    yield return null;
                else {
                    if(_coroutine != null && _coroutine.MoveNext()) {
                        yield return _coroutine.Current;
                    }
                    else {
                        Running = false;
                    }
                }
            }

            if (Time.frameCount - startedFrame == 0)
            {
                yield return null;
            }
            
            if (!Stopped) _onFinished?.Invoke();
        }

        public CoroutineState OnComplete(Action onFinishedCallback)
        {
            _onFinished = onFinishedCallback;

            return this;
        }
    }
    
    public class CoroutineHandler : MonoBehaviour
    {
        private static CoroutineHandler _instance;

        private static CoroutineHandler Instance => _instance != null ? _instance : (_instance = new GameObject("CoroutineHandler").AddComponent<CoroutineHandler>());

        public static CoroutineState HandleCoroutine(IEnumerator coroutine)
        {
            return Instance.HandleCoroutine(coroutine);
        }

        public static CoroutineState WaitForFixedUpdate()
        {
            return Instance.WaitForFixedUpdate();
        }
	
        public static CoroutineState WaitForSeconds(float time)
        {
            return Instance.WaitForSeconds(time);
        }

        public static CoroutineState WaitForEndOfFrame()
        {
            return Instance.WaitForEndOfFrame();
        }

        public static CoroutineState WaitUntil(WaitUntilDelegate callback)
        {
            return Instance.WaitUntil(callback);
        }

        public static CoroutineState UpdateForSeconds(float sec, Action<float, float> callback)
        {
            return Instance.UpdateForSeconds(sec, callback);
        }
    }
    
    public static class CoroutineHandlerExtension
    {
        
        private static readonly Dictionary<MonoBehaviour, List<CoroutineState>> coroutineStates =
            new Dictionary<MonoBehaviour, List<CoroutineState>>();
        
        public static CoroutineState HandleCoroutine(this MonoBehaviour behaviour, IEnumerator coroutine)
        {
            var state = new CoroutineState(behaviour, coroutine);

            if (coroutineStates.ContainsKey(behaviour))
            {
                coroutineStates[behaviour].Add(state);
            }
            else
            {
                coroutineStates.Add(behaviour, new List<CoroutineState>{state});
            }
            
            return state;
        }

        public static void StopCoroutines(this MonoBehaviour behaviour)
        {
            if (coroutineStates.ContainsKey(behaviour))
            {
                var coroutineStates = CoroutineHandlerExtension.coroutineStates[behaviour];
                coroutineStates.ForEach(state => state.Stop());
                coroutineStates.Clear();
            }
        }
    
        private static IEnumerator WaitingCoroutines<T>(T yieldType) where T : YieldInstruction
        {
            yield return yieldType;
        }
    
        public static CoroutineState WaitForFixedUpdate(this MonoBehaviour behaviour)
        {
            return HandleCoroutine(behaviour, WaitingCoroutines(new WaitForFixedUpdate()));
        }
	
        public static CoroutineState WaitForSeconds(this MonoBehaviour behaviour, float time)
        {
            return HandleCoroutine(behaviour, WaitingCoroutines(new WaitForSeconds(time)));
        }

        public static CoroutineState WaitForEndOfFrame(this MonoBehaviour behaviour)
        {
            return HandleCoroutine(behaviour, WaitingCoroutines(new WaitForEndOfFrame()));
        }
    
        private static IEnumerator UpdateWhile(UpdateWhileDelegate callback)
        {
            var timeElapsed = 0f;

            while (callback(Time.deltaTime, timeElapsed))
            {
                yield return null;

                timeElapsed += Time.deltaTime;
            }
        }

        private static IEnumerator FixedUpdateWhile(UpdateWhileDelegate callback)
        {
            var timeElapsed = 0f;

            do
            {
                yield return new WaitForFixedUpdate();

                timeElapsed += Time.fixedDeltaTime;

            } while (callback(Time.fixedDeltaTime, timeElapsed));
        }
    
        public static CoroutineState WaitUntil(this MonoBehaviour behaviour, WaitUntilDelegate callback)
        {
            return HandleCoroutine(behaviour, UpdateWhile((deltaTime, time) => callback()));
        }

        public static CoroutineState UpdateForSeconds(this MonoBehaviour behaviour, float sec, Action<float, float> callback)
        {
            return HandleCoroutine(behaviour, UpdateWhile((deltaTime, timeElapsed) =>
            {
                float timeElapsedClamped = Mathf.Clamp(timeElapsed, 0, sec);
                
                callback(deltaTime - (timeElapsed - timeElapsedClamped), timeElapsedClamped);

                return timeElapsed < sec;
            }));
        }
        
        public static CoroutineState FixedUpdateForSeconds(this MonoBehaviour behaviour, float sec, Action<float, float> callback)
        {
            return HandleCoroutine(behaviour, FixedUpdateWhile((deltaTime, timeElapsed) =>
            {
                float timeElapsedClamped = Mathf.Clamp(timeElapsed, 0, sec);
                
                callback(deltaTime - (timeElapsed - timeElapsedClamped), timeElapsedClamped);

                return timeElapsed < sec;
            }));
        }
        
    }
}