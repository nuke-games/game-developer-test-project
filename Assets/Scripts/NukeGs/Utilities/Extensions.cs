﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Utilities
{
    public static class ConversionExtensions
    {
        public static Vector2 Rotate(this Vector2 v, float degrees) {
            var radians = degrees * Mathf.Deg2Rad;
            var sin = Mathf.Sin(radians);
            var cos = Mathf.Cos(radians);
         
            var tx = v.x;
            var ty = v.y;
 
            return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
        }
        
        public static Vector3 Rotate(this Vector3 v, float degrees, Vector3 axis)
        {
            return Quaternion.AngleAxis(degrees, axis) * v;
        }

        public static Vector3 ProjectedVector(this Vector3 a, Vector3 b)
        {
            return (Vector3.Dot(b,a)*b) / b.sqrMagnitude;
        }
        
        public static Vector2 ProjectedVector(this Vector2 a, Vector2 b)
        {
            return Vector2.Dot(b, a) / b.sqrMagnitude * b;
        }
        
        public static Vector3 RotatePointAroundPointYAxis(this Vector3 point, float angle, Vector3 pivotPoint)
        {
            var newPoint = point - pivotPoint;
            float newX = newPoint.x * Mathf.Cos(angle * Mathf.Deg2Rad) - newPoint.z * Mathf.Sin(angle * Mathf.Deg2Rad);
            float newZ = newPoint.x * Mathf.Sin(angle * Mathf.Deg2Rad) + newPoint.z * Mathf.Cos(angle * Mathf.Deg2Rad);

            newPoint.x = newX;
            newPoint.z = newZ;
            
            return newPoint + pivotPoint;
        }
    }

    public static class GameObjectExtensions
    {
        public static void SetLayer(this GameObject gObject, int layer, bool includeChildren = true)
        {
            gObject.layer = layer;

            if (!includeChildren) return;

            foreach (Transform child in gObject.transform)
            {
                child.gameObject.SetLayer(layer);
            }
        }

        public static GameObject FindChildWithTag(this GameObject gObject, string tag)
        {
            Transform transform = gObject.transform;
            foreach (Transform tr in transform)
            {
                if (tr.CompareTag(tag))
                {
                    return tr.gameObject;
                }
            }

            return null;
        }
    }

    public static class TransformExtensions
    {
        public static void SetLossyScale(this Transform transform, Vector3 scale)
        {
            var lossyScale = transform.lossyScale;
            var localScale = transform.localScale;
            var targetLocalScale = new Vector3();

            targetLocalScale.x = (localScale.x / lossyScale.x) * scale.x;
            targetLocalScale.y = (localScale.y / lossyScale.y) * scale.y;
            targetLocalScale.z = (localScale.z / lossyScale.z) * scale.z;

            transform.localScale = targetLocalScale;
        }

        public static void SetLocalScaleX(this Transform transform, float scaleX)
        {
            var currentScale = transform.localScale;
            currentScale.x = scaleX;
            transform.localScale = currentScale;
        }
        
        public static void SetLocalScaleY(this Transform transform, float scaleY)
        {
            var currentScale = transform.localScale;
            currentScale.y = scaleY;
            transform.localScale = currentScale;
        }
        
        public static void SetLocalScaleZ(this Transform transform, float scaleZ)
        {
            var currentScale = transform.localScale;
            currentScale.z = scaleZ;
            transform.localScale = currentScale;
        }
    }

    public static class ListExtensions
    {
        public static T RandomItem<T>(this List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }

        public static T LastItem<T>(this List<T> list)
        {
            return list[list.Count - 1];
        }
        
        public static List<int> Shuffle<T>(this IList<T> list)
        {
            var newLocation = new List<int>();

            var n = list.Count;

            for (var i = 0; i < list.Count; i++)
            {
                newLocation.Add(i);
            }

            while (n > 1)
            {
                n--;
                var k = Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;

                var tempValue = newLocation[n];
                newLocation[n] = newLocation[k];
                newLocation[k] = tempValue;
            }

            return newLocation;
        }
    }

    public static class FloatExtensions
    {
        public static bool EqualTo(this float a, float b)
        {
            return Mathf.Abs(a-b) < Mathf.Epsilon;
        }

        public static bool GreaterThan(this float a, float b)
        {
            return a-b > Mathf.Epsilon;
        }
        
        public static bool LessThan(this float a, float b)
        {
            return a-b < Mathf.Epsilon;
        }
    }

    public static class ArrayExtensions
    {
        public static LayerMask ConvertToLayerMask(this int[] array)
        {
            LayerMask layerMask = 0;
            foreach (var layerIndex in array)
            {
                var newLayer = (LayerMask) 1 << layerIndex;
                layerMask |= newLayer;
            }

            return layerMask;
        }
    }

    public static class AnimationExtensions
    {
        public static void OnAnimationStep(this Animator animator, int layer, string stateName, float normalizedTime, Action callback)
        {
            var currentStateInfo = animator.GetCurrentAnimatorStateInfo(layer);
            float currentNormalizedTime = currentStateInfo.normalizedTime;

            CoroutineHandler.WaitUntil(() =>
            {
                currentStateInfo = animator.GetCurrentAnimatorStateInfo(layer);
                
                if (!currentStateInfo.IsName(stateName)) return true;
                
                float previousNormalizedTime = currentNormalizedTime;
                currentNormalizedTime = currentStateInfo.normalizedTime;
                return !(previousNormalizedTime < normalizedTime && currentNormalizedTime >= normalizedTime);
            }).OnComplete(callback);
        }

        public static float GetClipLength(this Animator animator, string clipName)
        {
            var clips = animator.runtimeAnimatorController.animationClips.ToList();

            return clips.Find(clip => clip.name == clipName).length;
        }
    }
}
